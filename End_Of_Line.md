# Lorem ipsum

## Lorem ipsum

1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
2. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit:

- Lorem ipsum dolor sit amet, consectetur adipiscing elit?
- Lorem ipsum dolor sit amet, consectetur adipiscing elit?
- Lorem ipsum dolor sit amet, consectetur adipiscing elit?
- Lorem ipsum dolor sit amet, consectetur adipiscing elit?
- Lorem ipsum dolor sit amet, consectetur adipiscing elit?
- Lorem ipsum dolor sit amet, consectetur adipiscing elit?
- Lorem ipsum dolor sit amet, consectetur adipiscing elit,
  ed do eiusmod tempor incididunt ut labore et dolore magna aliqua?

Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

```plantuml
@startuml

title "Test System Context Diagram"

!include <material/common>
!include <material/desktop_classic>
!include <material/server>
!include <material/ev_station>
!include <material/car>
!include <C4/C4_Context>

Person(operator, "Operator")
System_Boundary(ts, "Test System") {
    System(t_fix, "Test Fixture", $sprite="ma_server")
    System(car, "Car", $sprite="ma_car")
    Rel_R(operator, t_fix, "operate", "keyboard / video / mouse")
    Rel_R(t_fix, car, "control")
}
System(station, "Station")

Rel(t_fix, station, "feed", "food")
Rel(t_fix, station, "perform actions", "REST-over-Ethernet")
Rel(car, station, "drink", "drunk")
Rel(car, station, "negotiate thirst", "screams")

@enduml
```

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Lorem ipsum

### Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum dolor sit amet, consectetur adipiscing elit
Lorem ipsum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit:

- Lorem ipsum dolor sit amet, consectetur adipiscing elit
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
- Lorem ipsum dolor sit amet, consectetur adipiscing elit
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
- Lorem ipsum dolor sit amet, consectetur adipiscing elit
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
 sed do
- Lorem ipsum dolor sit amet, consectetur adipiscing elit
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
- Lorem ipsum dolor sit amet, consectetur adipiscing elit
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
 sed do eiusmod

### Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod:

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
  tempor incididunt ut labore
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
  tempor incididunt ut labore
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
  tempor incididunt ut labore
- Lorem ipsum dolor sit amet, consectetur adipiscing
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
  tempor incididunt ut labore
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
  tempor incididunt ut labore

### Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.
Lorem ipsum dolor sit amet, consectetur adipiscing.

Lorem ipsum dolor

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
  tempor
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
  tempor incididunt ut labore

Lorem ipsum

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet

Lorem ipsum dolor

- Lorem ipsum dolor sit amet

Lorem ipsum dolor

- Lorem ipsum dolor sit amet, consectetur adipiscing
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
  tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur
  adipiscing elit, sed do eiusmod.

Lorem ipsum dolor

- Lorem ipsum dolor sit amet, consectetur adipiscing
- Lorem ipsum dolor sit amet, consectetur

### Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet

Lorem ipsum

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet

Lorem ipsum dolor sit amet

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet

Lorem ipsum dolor sit amet

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet

Lorem ipsum dolor sit amet

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do

### Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing 

Lorem ipsum

- Lorem ipsum dolor sit

Lorem ipsum

- Lorem ipsum dolor sit

### Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet

```plantuml
@startuml

state "System Boot" as boot
state "End of Line" as eol
state "Peripheral Firmware Programming" as fw_prog
state "Recovery" as recover
state "Mandatory Configuration" as config
state "Operational" as operational


@enduml
```

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis

- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet,
- Lorem ipsum dolor sit amet

### Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor

## Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit

## Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit

```plantuml
@startuml
|Operator|
start
:Start test;
|Test System|
:Do something;
stop
@enduml
```

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor

### Problematic diagram

```plantuml
@startuml

title Initialize REST API

actor "Operator" as opr
participant "EoL Test System" as t_sys

box "Charging Station" #GreenYellow
    participant "IPMP-over-CAN" as can
    participant "REST-over-Ethernet" as rest
    participant "EV connector" as ev
end box

t_sys ->> rest: ""POST /auth/login {""\n""  "password": $password""\n""}""
activate rest
alt
    rest --> t_sys: ""200 {""\n""  "access_token": $access_token""\n""}""
else #Coral
    rest --> t_sys: ""401 {""\n""  "code": $error_code""\n""  "message": $error_message""\n""}""
    deactivate rest
    opr <- t_sys: Password error
end

t_sys -> t_sys: Set HTTP header: ""Authorization: Bearer $access_token""
t_sys ->> rest: ""PUT /sys-information/date-time {""\n""  "timestamp": $seconds_since_epoch""\n""}""
activate rest
alt
    rest --> t_sys: ""200""
else #Coral
    rest --> t_sys: ""401 {""\n""  "code": $error_code""\n""  "message": $error_message""\n""}""
    deactivate rest
    opr <- t_sys: Date time error
end

@enduml
```
